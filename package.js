Package.describe({
  name: 'ubersoft:dragula',
  version: '3.7.2_2',
  summary: 'Wrapper package for Dragula 3.7.2',
  git: 'https://bitbucket.org/ubersoftinc/meteor-dragula.git',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4.3.1');
  // api.use('ecmascript');
  api.use('jquery', 'client');
  api.addFiles(['dragula.min.js', 'dragula.min.css'], 'client');
});

Package.onTest(function(api) {
  // api.use('ecmascript');
  api.use('tinytest');
  api.use('jquery', 'client');
  api.use('ubersoft:dragula');
  api.mainModule('dragula-tests.js');
});
