// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by dragula.js.
import { name as packageName } from "meteor/ubersoft:dragula";

// Write your tests here!
// Here is an example.
Tinytest.add('dragula - example', function (test) {
  test.equal(packageName, "dragula");
});
