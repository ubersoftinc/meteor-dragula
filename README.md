# Example
html
```
<div id="dragula-container">
  <div>item 1</div>
  <div>item 2</div>
  <div>item 3</div>
</div>
```
js
```
window.dragula([document.querySelector('#dragula-container')])
```
